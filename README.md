## Rezet (Test Task)

#### Start Back-End
```bash
  git clone https://github.com/AlexKorob/rezet-test.git
  cd backend
  python3.7 -m venv ./venv
  . venv/bin/activate
  pip3 install -r requirements.txt
  python3.7 ./manage.py migrate
  python3.7 ./manage.py runserver
```

#### Run backend Tests

```bash
  python3.7 ./manage.py test
```

##### API doc: http://localhost:8000/swagger/

---
---
---
# Front-End

#### Project setup
```
cd frontend
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```
Then sign in with:<br/>
username: alex<br/>
password: 123

#### Compiles and minifies for production
```
npm run build
```
#### Lints and fixes files
```
npm run lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
