import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Cart from '../views/Cart.vue'
import Shipping from '../views/Shipping.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: "/",
    redirect: Login
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
      path: "/cart",
      name: "Cart",
      meta: {
        requiresAuth: true
      },
      component: Cart
  },
  {
      path: "/shipping",
      name: "Shipping",
      meta: {
        requiresAuth: true
      },
      component: Shipping,
      props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
