import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'

Vue.use(Vuelidate)
Vue.use(VueAxios, Axios)

Vue.config.productionTip = false

let hostname = "http://localhost:8000"
Vue.prototype.hostname = hostname
Vue.prototype.$URLLogin = hostname + "/api-token-auth/"
Vue.prototype.$URLCartItems = hostname + "/cart/"
Vue.prototype.$URLShipping = hostname + "/shipping/"


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem("token")) {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  router,
  methods: {
    setAuthTokenAtHeaders(token) {
      Vue.prototype.axios.defaults.headers.common['Authorization'] = `Token ${token}`;
    }
  },
  created: function() {
    let token = localStorage.getItem('token');
    if (token) {
      this.setAuthTokenAtHeaders(token);
    }
  },
  render: h => h(App)
}).$mount('#app')
